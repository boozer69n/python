#!usr/bin/python
	# *You seem to REALLY like recursion, haha*
#imports
import RPi.GPIO as GPIO
import time
import random

#5x12=red
#6x13=blue
#17x20=yellow
#18x21=green
button_list = [12,13,20,21]# *Same as below*
led_list = [5,6,17,18]   # *<-- You could set up a checking function that corresponds simply to a list 0-3 instead of this, but your way should work in theory*

#variables
levels = 32   # *I think that the levels have to increment during play, so level should be 1 then 2 then 3, etc.
			  # *This would mean that you need to change your iterative function that uses level to increment level and have another variable to compare it to (i.e. current_level)
buttons = 4

#lists
seq_list = []
game_list = []

#GPIO setup
GPIO.setmode(GPIO.BCM)
for i in button_list:
    GPIO.setup(i, GPIO.IN)
    GPIO.add_event_detect(i, GPIO.RISING)
for j in led_list:
    GPIO.setup(j, GPIO.OUT)

#clears lists and creates a new one
def new_seq():
	cur_level = 0
    for z in seq_list:
        seq_list.pop()		# *<--to delete total arrays you can do something like:  del seq_list[:]   this will save on recursion*
    for y in game_list:
        game_list.pop()		# *<-- same thing here*
    for x in range(0,levels):	# this should work, but if not you could also use a counter (i.e. counter = levels: while counter > 0 add to list, reduce counter by one)
        seq_list.append(led_list[random.randrange(0,buttons-1)])

#checks which button was pushed
def but_chk(r):
    if r == led_list[0]:
        return GPIO.event_detect(button_list[0])
    elif r == led_list[1]:
        return GPIO.event_detect(button_list[1])
    elif r == led_list[2]:
        return GPIO.event_detect(button_list[2])
    elif r == led_list[3]:
        return GPIO.event_detect(button_list[3])
    else:
        print "number out of bounds"
        return False

#pausing until a button is pressed
def but_press():
    should_exit = False
    while should_exit == False:
        if (GPIO.event_detect(button_list[0]) == True) or (GPIO.event_detect(button_list[1]) == True) or (GPIO.event_detect(button_list[2]) == True) or (GPIO.event_detect(button_list[3]) == True):
            should_exit = True

#blink the led that corresponds tot he passed in argument
def blink(l):
    GPIO.output(l, True)
    time.sleep(0.2)
    GPIO.output(l, False)
    time.sleep(0.2)

#initiates the sequence blinking loop
def blink_loop():
    for i in game_list:
        blink(i)

#updates sequence
def update():
	cur_level += 1
    game_list.append(seq_list.pop())
	
#winning function
def you_win():		# *<-- I would rewrite this to be simpler.
	# print "You Win!!!!"
	# for x in led_list:
		# for y in led_list:
			# GPIO.output(random.randrange(0,buttons-1), tf())
			# time.sleep(0.1)
	# for z in led_list:
		# GPIO.output(z, False)
		
	# ***********For example:
	win = 3
	while (win > 0):
		GPIO.output(5, True)
		GPIO.output(6, True)
		GPIO.output(17, True)
		GPIO.output(18, True)
		time.sleep(0.2)
		GPIO.output(5, False)
		GPIO.output(6, False)
		GPIO.output(17, False)
		GPIO.output(18, False)
		time.sleep(0.1)
		win -= 1

#random true/false
def tf():
	var = random.randrange(0,1)
	if var == 0:
		return True
	else:
		return False
		
#loosing function
def you_lose():
	print "You Loose!!"
	for x in led_list:
		GPIO.output(x, True)
		# time.sleep(0.1)    <-- Got rid of this, as all lights should turn on at once
	for y in led_list:
		GPIO.output(y, False)
		time.sleep(0.1)
	# This function should either initiate a new game or have new game after

#starting a new game
def new_game():
    new_seq()
    while True:		# *This should be a variable you can change in functions, then you can easily terminate or recycle the game
        btn = True	# *<-- what is this variable being used for?
        update()
        blink_loop()
		for x in game_list:
			but_press()
			if but_chk(x) == False:
				you_lose()
				new_game()
			elif cur_level == levels:
				you_win()
				new_game()
			


#   // Loop
#      // Generate random sequence, store in array
#      // Output sequence
#      // Wait for any switch press
#         // If switch does not correspond to current light in sequence
#          // Start over
#        // If switch corresponds to current light in sequence
#          // Increment sequence
#          // If at end of sequence




# Another (simpler, in my opinion) way to do the button press detection is by using the following function:
# GPIO.setup(x, GPIO.IN, pull_up_down=GPIO.PUD_UP)
# then using something like this in the function
# green_pushed = GPIO.input(5)
#Then something like:
#if green_pushed == False:
#        print('Green Pressed')
#        time.sleep(0.3) # <- bounceback
#        user_input.append(0)
#        counter += 1

# Btw, this function uses a False input as the button press (as the button is tied to a ground) I will send you a picture of my setup if I can find it
# This way you can use a function that checks against all of the colors