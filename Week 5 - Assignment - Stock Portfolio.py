#In this assignment you will build a stock portfolio manager. The manager program
#   will consist of 3 dictionaries.
#
#The first dictionary, called Names, maps the stock symbol to the company name
#   (example: "GM" maps to "General Motors").
#The second dictionary, called Prices, maps the stock symbol to a list of 2
#   floating point numbers corresponding to the buy price (the price the user
#   paid for the stock) and the current market price (the price the user could
#   sell the stock for today).
#The third dictionary, called Exposure, maps the stock symbol to a list of 2
#   floating point numbers, corresponding to the number of shares purchased,
#   and the risk associated with holding onto the stock (i.e. How likely the
#   stock is to gain value in the future).
#
#Your program should consist of the following functions:
#   AddName - Asks the user for a Stock Symbol and Name pairing then adds it
#       to the Names dictionary.
#   AddPrices - Takes a Stock Symbol as an input parameter, then asks the user
#       for the Buy price and the Current price of the corresponding stock,
#       adding them to the Prices dictionary.
#   AddExposure - Takes a Stock Symbol as an input parameter, then asks the user
#       for the Risk and Shares of the corresponding stock, adding them to the
#       Exposure dictionary.
#   AddStock - Calls AddName, AddPrices, and AddExposure to add a new stock to
#       the portfolio.
#   GetSale - Finds the maximum expected value of selling a stock. The expected
#       sale value of a stock is the current profit minus the future value of
#       the stock:
#       Expected Sale value = ( ( Current Price - Buy Price ) - Risk * CurrentPrice ) * Shares
#   The GetSale function should calculate this value for each stock in the
#       portfolio, and return the stock symbol with the highest expected sale value.
#   Main - Should take no arguments, but present a menu item consisting of
#       "1. Add Stock", "2. Recommend Sale" and "3. Exit".
#       If the user selects '1,' the Add Stock function is called, and when it is
#                               complete, the menu is presented again. If the user selects
#                           '2,' the Symbol of the stock corresponding to the highest
#                               expected value (returned by GetSale) should be displayed,
#                               and the menu presented after completion. If the user selects
#                           '3', the program should end.
################################################
stockSym = 'TBD'
################################################
names = {'GOOG':'Google',
         'HMSY':'HMS Holdings Corp',
         'ISRG':'Intuitive Surgical Inc.',
         'HIBB':'Hibbett Sports Incorporated',
         'EBAY':'eBay Incorporated',
         'ROST':'Ross Stores Incorporated',
         'AMZN':'Amazon.Com Inc.',
         'CTSH':'Cognizant Technology Solutions',
         'EVOL':'Evolving Systems Inc.',
         'EGOV':'Nic Inc.'}
prices = {'GOOG':[875.04, 1152.18],
         'HMSY':[22.56, 29.19],
         'ISRG':[498.31, 640.35],
         'HIBB':[57.30, 73.57],
         'EBAY':[51.29, 64.89],
         'ROST':[64.86, 81.75],
         'AMZN':[273.99, 339.53],
         'CTSH':[62.98, 77.27],
         'EVOL':[6.56, 8.01],
         'EGOV':[16.13, 19.41]}
exposure = {'GOOG':[20, 10.25],
            'HMSY':[50, 5.16],
            'ISRG':[40, 12.01],
            'HIBB':[46, 9.09],
            'EBAY':[105, 13.35],
            'ROST':[218, 23.81],
            'AMZN':[87, 9.47],
            'CTSH':[529, 15.42],
            'EVOL':[26, 15.60],
            'EGOV':[85, 18.94]}
##################################################
def addName():
    stockNam = input ('Name of stock: ')
    names[stockSym] = stockNam
##################################################
def addPrice():
    stockBuy = input ('Buy price of the stock: ')
    stockCur = input ('Current price of the stock: ')
    prices[stockSym] = [float(stockBuy), float(stockCur)]
##################################################
def addExposure():    
    stockShar = input ('Number of shares: ')
    stockRisk = input ('Risk percentage(%): ')
    exposure[stockSym] = [float(stockShar), float(stockRisk)]
##################################################
def addStock():
    addName()
    addPrice()
    addExposure()
##################################################
def getSale():
    highest = 0
    base = 0
    for pricesLP in prices:
        pricesBuy = prices[pricesLP][0]
        pricesCur = prices[pricesLP][1]
        exposureShar = exposure[pricesLP][0]
        exposureRisk = exposure[pricesLP][1]
        expected = (((float(pricesCur) - float(pricesBuy)) - (float(exposureRisk)/100) *
                     float(pricesCur)) * float(exposureShar))
        if base < expected:
            base = expected
            highest = pricesLP
    print ('')
    print ('Symbol:',highest)
    print ('Name:',names[highest])
##################################################
'''USE FOR TESTING'''
#addName()
#addPrice()
#addExposure()
#addStock()
#getSale()
##################################################
shouldExit = False
while shouldExit == False:
    print ('')
    print ('Feel free to choose another.')
    print ('1 - Add Stock')
    print ('2 - Recommend Sale')
    print ('3 - Exit')
    answer = input ('Pick a number: ')
    if str(answer) == '1':
        stockSym = input ('Symbol of the stock: ')
        addStock()
    elif str(answer) == '2':
        getSale()
    elif str(answer) == '3':
        print ('All done!!')
        shouldExit = True
    else:
        print ('')
        print ('Dont be dumb!')
##################################################
