from random import randint

#############################################
def fun1(list1):
    fun1List = []
    for fun1num in list1:
        try:
            check = fun1List.index(fun1num)
        except ValueError:
            fun1List.append(fun1num)
    fun1List.sort()
    print ("")
    print(fun1List)
    print ("")
#############################################
def fun2(list1):
    fun2List = []
    for fun2num in list1:
        fun2List.append(fun2num)
    fun2List.sort()
    if list1 == fun2List:
        print ("")
        print ('True')
        print ("")
    else:
        print ("")
        print ('False')
        print ("")
#############################################
def fun3(list1):
    try:
        fun3Total = 0
        for fun3num in list1:
            fun3Total = fun3Total + fun3num
        print ("")
        print ("Total =",fun3Total)
        print ("")
    except TypeError:
        print ("")
        print ("You entered words not numbers.")
        print ("")
#############################################
def fun4(list1):
    list1.reverse()
    print("")
    print (list1)
    print ("")
    list1.reverse()
#############################################
def fun5(list1, list2):
    fun5List = []
    for fun5num in list1:
        fun5List.append(fun5num)
    for fun5num in list2:
        fun5List.append(fun5num)
    fun5List.sort()
    print ("")
    print (fun5List)
    print ("")    
#############################################
def fun6():
    print ("##################################")
    print ("            Function 1")
    print ("##################################")
    fun1(list1)
    print ("##################################")
    print ("            Function 2")
    print ("##################################")
    fun2(list1)
    print ("##################################")
    print ("            Function 3")
    print ("##################################")
    fun3(list1)
    print ("##################################")
    print ("            Function 4")
    print ("##################################")
    fun4(list1)
    print ("##################################")
    print ("            Function 5")
    print ("##################################")
    fun5(list1, list2)
    print ("##################################")
    print ("")
#############################################
num1 = 0
num2 = 0

list1 = []
list2 = []

print ("Would you like to generate random numbers or your own input?")
work = input ("(R)andom or (S)elf input: ")

if (work.upper()) == 'R':
    list1Input = input ("How many random numbers do you want in the first list? ")
    list1Input = int(list1Input)
    list2Input = input ("How many random numbers do you want in the second list? ")
    list2Input = int(list2Input)

    for num1 in range (list1Input):
        num1 = randint (0, 100)
        list1.append (num1)
    for num2 in range (list2Input):
        num2 = randint (0, 100)
        list2.append (num2)
elif (work.upper()) == 'S':
    done = False
    print ("List 1")
    while done == False:
        addition = input ("'exit' when done - Input: ")
        if addition == 'exit':
            done = True
        else:
            list1.append(addition)
    done = False
    print ("List 2")
    while done == False:
        addition = input ("'exit' when done - Input: ")
        if addition == 'exit':
            done = True
        else:
            list2.append(addition)
        
shouldExit = False
while shouldExit == False:
    print ("")
    print ("Type 'exit' when finished.")
    print ("1 - Displays the list minus duplicates")
    print ("2 - Displays whether or not the list is sorted")
    print ("3 - Displays the sum of all the numbers in the list")
    print ("4 - Displays the list in reverse order")
    print ("5 - Displays the 2 lists combined and sorted")
    print ("6 - Displays all 5 functions")
    answer = input ("Which function between 1 & 6 would you like to see? ")
    print ("")
    if str(answer) == "1":
        fun1(list1)
    elif str(answer) == "2":
        fun2(list1)
    elif str(answer) == "3":
        fun3(list1)
    elif str(answer) == "4":
        fun4(list1)
    elif str(answer) == "5":
        fun5(list1, list2)
    elif str(answer) == "6":
        fun6()
    elif str(answer) == "exit":
        print ("All done!!")
        shouldExit = True
    else:
        print ("Dont be dumb!")



