# Ian Tyler
# Simon Game

import RPi.GPIO as GPIO
import os
import time
import random

# GPIO.cleanup() used here as well for testing

# setup GPIO board and pins
GPIO.setmode(GPIO.BOARD)
GPIO.setup(12, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(16, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(18, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(22, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(7, GPIO.OUT)
GPIO.setup(11, GPIO.OUT)
GPIO.setup(13, GPIO.OUT)
GPIO.setup(37, GPIO.OUT)

# initialize variables and arrays
done = False
counter = 0
sequence = 0
level = 1
color = 0
win = 0
lose = 0
game_over = 0
color_sequence = []
user_input = []

# body loop for game
while (done == False):

    # resets sequence, counter, and win for next level
    sequence = 0
    counter = 0
    win = 0

    # makes sure that all lights are off before level
    GPIO.output(7, False)
    GPIO.output(11, False)
    GPIO.output(13, False)
    GPIO.output(37, False)
    time.sleep(.5)

    # color sequence loop, increments with level
    while (sequence < level):

        # uses random integer for light illuminated
        color = random.randint(0, 3)

        # for color green
        if color == 0:
            
            # briefly shows color
            GPIO.output(7, True)
            time.sleep(1)
            GPIO.output(7, False)
            time.sleep(.2)
            
            # adds color value to sequence array, increments count
            color_sequence.append(0)
            sequence += 1

        # for color yellow
        elif color == 1:

            # briefly shows color
            GPIO.output(11, True)
            time.sleep(1)
            GPIO.output(11, False)
            time.sleep(.2)

            # adds color value to sequence array, increments count
            color_sequence.append(1)
            sequence += 1

        # for color red
        elif color == 2:

            # briefly shows color
            GPIO.output(13, True)
            time.sleep(1)
            GPIO.output(13, False)
            time.sleep(.2)

            # adds color value to sequence array, increments count
            color_sequence.append(2)
            sequence += 1

        # for color blue
        elif color == 3:

            # briefly shows color
            GPIO.output(37, True)
            time.sleep(1)
            GPIO.output(37, False)
            time.sleep(.2)

            # adds color value to sequence array, increments count
            color_sequence.append(3)
            sequence += 1


    # print color_sequence here for testing*******************

    # user input loop, increments with level
    while (counter < level):

        # sets pushed values for switch
        green_pushed = GPIO.input(12)
        yellow_pushed = GPIO.input(16)
        red_pushed = GPIO.input(18)
        blue_pushed = GPIO.input(22)

        # if green is pressed, adds color value to user_input array, increments count
        if green_pushed == False:
            print('Green Pressed')
            time.sleep(0.3) # <- bounceback
            user_input.append(0)
            counter += 1

        # if yellow is pressed, adds color value to user_input array, increments count  
        if yellow_pushed == False:
            print('Yellow Pressed')
            time.sleep(0.3) # <- bounceback
            user_input.append(1)
            counter += 1
            
        # if red is pressed, adds color value to user_input array, increments count
        if red_pushed == False:
            print('Red Pressed')
            time.sleep(0.3) # <- bounceback
            user_input.append(2)
            counter += 1
            
        # if blue is pressed, adds color value to user_input array, increments count
        if blue_pushed == False:
            print('Blue Pressed')
            time.sleep(0.3) # <- bounceback
            user_input.append(3)
            counter += 1


    # winning sequence
    if (color_sequence == user_input):

        # set to 4, as 1 will be used later
        win = 4

        # loop win until 1, blinks lights 3 times
        while (win > 1):
            GPIO.output(7, True)
            GPIO.output(11, True)
            GPIO.output(13, True)
            GPIO.output(37, True)
            time.sleep(.5)
            GPIO.output(7, False)
            GPIO.output(11, False)
            GPIO.output(13, False)
            GPIO.output(37, False)
            time.sleep(.2)
            win -= 1 # <- decrements win
            

    # losing sequence
    elif (color_sequence != user_input):
        
        # all lights on, turn off in numeric order (green, yellow, red and blue) respectively
        GPIO.output(7, True)
        GPIO.output(11, True)
        GPIO.output(13, True)
        GPIO.output(37, True)
        time.sleep(1)
        GPIO.output(7, False)
        time.sleep(.5)
        GPIO.output(11, False)
        time.sleep(.5)
        GPIO.output(13, False)
        time.sleep(.5)
        GPIO.output(37, False)
        time.sleep(.5)


    # win increments level
    if (win == 1):
        level += 1

    # lose restarts from beginning
    elif (win == 0):
        level = 1

    # reset color_sequence and user_input arrays for next level
    del color_sequence[:]
    del user_input[:]

    # I added a game-win sequence with a light pattern (for fun)
    if (level > 10):
        
        game_over = 4
        print("You win! Nice job!") # <- victory message
        
        while (game_over > 0):

            # color pattern looped 4 times
            GPIO.output(7, True)
            GPIO.output(11, True)
            GPIO.output(13, True)
            GPIO.output(37, True)
            time.sleep(1)
            GPIO.output(11, False)
            GPIO.output(37, False)
            time.sleep(1)
            GPIO.output(11, True)
            GPIO.output(37, True)
            GPIO.output(7, False)
            GPIO.output(13, False)
            time.sleep(1)
            GPIO.output(11, False)
            GPIO.output(37, False)
            time.sleep(1)
            game_over -= 1
            
        done = True # <- ends game loop


# clears board assignments at end of program           
GPIO.cleanup()
exit(0)
