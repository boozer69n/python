#!usr/bin/python

#imports
import RPi.GPIO as GPIO
import time
import send_email

#GPIO setup
GPIO.setmode(GPIO.BOARD)
GPIO.setup(32, GPIO.IN)
GPIO.add_event_detect(32, GPIO.RISING)

#loops
send = False
while True:
    if GPIO.event_detected(32):
        if send:
            send = False
        else:
            send = True        
    if send:
        print 'sending'
        send_email.send_mail('boozer69n@gmail.com','boozer69n@cox.net', 'passwordgoeshere', 'Assignment 2.2', 'Unit 2: Week 2: Hardware Dev - Assignment 2.2: Button Input - Clinton Booze')
        send = False
        time.sleep(2)
    else:
        print 'holding'
        time.sleep(2)
