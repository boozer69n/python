#!usr/bin/python

#imports
import RPi.GPIO as GPIO
import time
from math import sqrt

#GPIO setup
GPIO.setmode(GPIO.BOARD)
GPIO.setup(11, GPIO.OUT)
GPIO.setup(12, GPIO.IN)
GPIO.add_event_detect(12, GPIO.RISING)

#blink number of times based off of argument
def blink(i):
    print i
    for x in range(0,i):
        GPIO.output(11, True)
        time.sleep(0.2)
        GPIO.output(11, False)
        time.sleep(0.2)

#return the next number in the fibonacci sequence
#source: http://en.literateprograms.org/Fibonacci_numbers_%28Python%29
def fib():
    a, b = 0, 1
    while 1:
        yield a
        a, b = b, a + b
a = fib()
a.next() #skipping the 0

#loops
on_off = False
while True:
    if GPIO.event_detected(12):
        if on_off:
            on_off = False
        else:
            on_off = True
    if on_off:
        blink(a.next())
        time.sleep(3)
    else:
        time.sleep(3)
