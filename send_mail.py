#!usr/bin/python

import smtplib
import sys
from os.path import basename
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

def send_mail(me, you, pwd, text):
    email_from = str(me)
    email_to = str(you)
    password = str(pwd)    

    msg = MIMEMultipart(
        From = email_from,
        To = email_to,
        )
    msg.attach(MIMEText(text))    

    server = smtplib.SMTP_SSL('smtp.gmail.com:465')
    server.login(email_from, password)
    server.sendmail(email_from, email_to, msg.as_string())
    server.close()


#send_mail('boozer69n@gmail.com',
#          'boozer69n@cox.net',
#          'gmail password was here, see school project directory',
#          'my text to send')
