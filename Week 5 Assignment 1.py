class Person:
    first_name = ""
    last_name = ""
    phone_number = ""

    def get_info(self):
        return self.first_name + " " + self.last_name + ": " + self.phone_number

class Friend(Person):
    email = ""
    birth_date = ""

    def get_info(self):
        return Person.get_info(self) + ", " + self.email + ", " + self.birth_date

contactList = []

def AskIfFriend():
    while True:
        userInput = input("Is this contact a friend (y/n)? ")
        userInput = userInput.strip()
        if (userInput == "y" or userInput == "Y"):
            return True
        elif (userInput == "n" or userInput == "N"):
            return False
    
def AddContact():
    if (AskIfFriend()):
        newContact = Friend()
    else:
        newContact = Person()

    newContact.first_name = input("Enter contact's first name: ")
    newContact.last_name = input("Enter contact's last name: ")
    newContact.phone_number = input("Enter contact's phone number: ")

    if (isinstance(newContact, Friend)):
        newContact.email = input("Enter contact's e-mail address: ")
        newContact.birth_date = input("Enter contact's birth date: ")

    contactList.append(newContact)
    print("\nContact Saved!\n")

def LookupContact():
    last_name = input("Enter the last name of the contact to look up: ")
    
    for contact in contactList:
        if (contact.last_name == last_name):
            if (isinstance(contact, Friend)):
                message = "Friend: "
            else:
                message = "Person: "
            print (message + contact.get_info())
    print("")

def GetAction():
    while True:
        print("What would you like to do?")
        print("   1. Add new contact")
        print("   2. Lookup contact")
        print("   3. Exit")
        userInput = input("? ")
        userInput = userInput.strip()
        
        if (userInput == "1" or userInput == "a" or userInput == "A"):
            return "new"
        elif (userInput == "2" or userInput == "l" or userInput == "L"):
            return "lookup"
        elif (userInput == "3" or userInput == "e" or userInput == "E"):
            return "exit"
        else:
            print("That is not a valid option. Please try again.\n")

def RunPhoneBook():
    action = ""
    while (action != "exit"):
        action = GetAction()
        if (action == "new"):
            AddContact()
        elif (action == "lookup"):
            LookupContact()
