############################################################################################
#                                      Word Madness                                        #
############################################################################################
#                                Created by Clinton Booze                                  #
#                                                                                          #
#                                       Purpose:                                           #
#  To count the Consonants or Vowels in a set of word(s) and to average them at the end    #
############################################################################################

############################################################################################
"""defining what is a vowel"""

#determining what letters are considered vowels
vowel = ('aeiouyAEIOUY')

############################################################################################
#asking what do you want to count either vowels or consonants

#variable to determine to end the loop
shouldExit = False
while shouldExit == False:
    print ("What do you want to count?")
    vc_count = input ("(V)owels or (C)onsonants? ")
    if (vc_count == "V") or (vc_count == "C") or (vc_count == "c") or (vc_count == "v"):
        shouldExit = True
#telling you that the value you entered is incorrect
    else:
        print ("\"" + vc_count + " \" is not a valid choice, Please choose \"V\" or \"C\"")
        
############################################################################################
#Asking what word(s) you want you count the vowels or consonants

#setting variable on whether or not to continue the loop
#counting how many words have been processes to be used for the average
#tallying the total amount of consonants of the word(s) being entered
#tallying the total amount of vowels of the word(s) being entered
more_word = True
word_count = 0
totalCon = 0
totalVow = 0

#continuing the loop if more words are entered
while more_word == True:
    shouldExit = False
    while shouldExit == False:
        word_input = input ("Please input your word (type 'exit' if you're done) ")
        if not (word_input == ""):
            shouldExit = True
        #if statements dont need else statements
        #exiting the section asking for more words when "exit" is typed
            if (word_input == "exit"):
                more_word = False
    #get this error message if you enter a blank
        else:
            print ("You must enter something")

############################################################################################
#getting the math for how many words and counting the vowels and consonants

#adding 1 to the number of words if you enter more words
    if more_word == True:
        word_count = word_count + 1
    #setting the variables back to 0 for the number of consonants and vowels
        numCon = 0
        numVow = 0
    #setting the variable char to each letter in the string defined as word_input
        for char in word_input:
        #checking to see if the char variable is within the vowel list above
            if char in vowel:
                numVow = numVow + 1
            else:
                numCon = numCon + 1
    #adding the vowels and consonants together to use later for the average                
        totalVow = totalVow + numVow
        totalCon = totalCon + numCon
        
        if (vc_count == "V") or (vc_count == "v"):
            print (word_input + " = " + str(numVow) + " Vowel(s)")
        else:
            print (word_input + " = " + str(numCon) + " Consonant(s)")
            
############################################################################################
#error checking
        
#       print ("vowel = ", totalVow)
#       print ('cons = ', totalCon)
#       print ("words = ", word_count)

############################################################################################
#doing the math and converting it into a whole number then converting it into a string

    else:
        if (vc_count == "V") or (vc_count == "v"):
            print ("Average = " + str(int(totalVow / word_count)))
        else:
            print ("Average = " + str(int(totalCon / word_count)))

############################################################################################
#                                         END                                              #
############################################################################################
