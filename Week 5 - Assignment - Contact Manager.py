#For this assignment you will create a simple contact manager application. This application
#   will track personal information about the user's friends and acquaintances. The user
#   will be able to enter information for new contacts and also lookup information for a contact.
#For this assignment you will create a simple class hierarchy. You will create an inheritance
#   relationship between two classes -- a Friend class and a Person class - Friend will
#   inherit Person.
#Your Person class will have the following attributes:
#    first_name
#    last_name
#    phone_number
#Your Friend class will have the following attributes:
#    email
#    birth_date
#Both your Person class and your Friend class will have a get_info method.
#    For the Person class, the get_info method will return a string with the
#       full name and phone number of the person.
#    For the Friend class, the get_info method will return a string with the
#       full name, phone number, email address, and birth date.
#The application will present a main menu to the user that will allow the user to add a
#   contact, lookup a contact by name, or exit the application.
#When the user chooses to add a contact, the application will ask the user if they want
#   to add a regular person or a friend. If the user wants to add a new regular person
#   contact then the application will ask for the first name, last, name and phone number
#   for that person. If the user wants to add a new friend contact then the application
#   will ask the user for the first name, last name, phone number, email address,
#   and birth date.
#When the user chooses to lookup a contact by name, the application will ask the user
#   for the last name, and then the application will display the full information for
#   all contacts with that last name.
###########################################################
#list
contactList = []
###########################################################
class People(object):
    firstName = ''
    lastName = ''
    phoneNumber = ''

    def getInfo(self):
        print ('Name:', self.firstName, self.lastName)
        print ('Number:', self.phoneNumber)
###########################################################
class Friend(People):
    email = ''
    birthDate = ''

    def getInfo(self):
        People.getInfo(self)
        print ('Email:', self.email)
        print ('Birthday:', self.birthDate)
###########################################################
def askFriend():
    shouldExit = False
    while shouldExit == False:
        friendInput = input ('Is this contact a friend (y/n)? ')
        if (friendInput == 'y'):
            shouldExit = True
            return True
        elif (friendInput == 'n'):
            shouldExit = True
            return False
        else:
            print ('')
            print ('Dont be dumb!')
            print ('')
###########################################################
def addContact():
    if askFriend():
        newContact = Friend()
    else:
        newContact = People()

    newContact.firstName = input ('First name: ')
    newContact.lastName = input ('Last name: ')
    newContact.phoneNumber = input ('Phone number: ')

    if (isinstance(newContact, Friend)):
        newContact.email = input ('Email: ')
        newContact.birthDate = input ('Birthday: ')

    contactList.append(newContact)
    print ('')
    print ('Contact added.')
    print ('')
###########################################################
def lookUp():
    lastNameLU = input ('Last name of contact: ')

    for contact in contactList:
        if contact.lastName == lastNameLU:
            if (isinstance(contact, Friend)):
                fType = 'Friend:'
            else:
                fType = 'Person:'
            print ('')
            print (fType)
            print (contact.getInfo())
    print ('')
###########################################################
shouldExit = False
while shouldExit == False:
    print ('')
    print ('1 - Add new contact')
    print ('2 - Look up contact')
    print ('3 - Exit')
    answer = input ('Pick a number: ')
    if str(answer) == '1':
        addContact()
    elif str(answer) == '2':
        lookUp()
    elif str(answer) == '3':
        print ('All done!!')
        shouldExit = True
    else:
        print ('')
        print ('Dont be dumb!')
###########################################################
