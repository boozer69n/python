##########################################################################
#                                  Functions                             #
##########################################################################
#                           Created by: Clinton Booze                    #
#                                                                        #
#                                   Purpose:                             #
#                 To run one of 8 possible functions assigned            #
##########################################################################

#setting the basic variables i will need later
print ("I need to get some info from you please: ")
print ("")
from random import randint
name = input ("What is your name? ")
major = input ("What is your major? ")
age = input ("What is your age? ")
count = 0
message = False
num1 = 0
capitalCount = 0
num2 = 0
saying = 0
phrase = input ("Please enter a phrase and include some capital letters: ")
alphabet = ('ABCDEFGHIJKLMNOPQRSTUVWXYZ')
print ("")

#function 1 displaying your name and major
def fun1():
    print ("")
    print ("Name: " + name)
    print ("Major: " + major)
    print ("")

#function 2 displaying your age as an integer
def fun2():
    print ("")
    print ("You are",int(age),"years young.")
    print ("")

#function 3 saying hello to you
def fun3():
    print ("")
    print ("Hello " + str(name))
    print ("")

#function 4 displaying your phrase the amount of times you request
def fun4():
    count = input ("Please input how many times you want to have your phrase repeated: ")
    count = int(count)
    print ("")
    while int(count) > 0:
        print (phrase)
        count = count - 1
    print ("")

#function 5 displaying the higher of two randomly generated numbers
def fun5():
    num1 = (randint (0, 100))
    num2 = (randint (0, 100))
    print ("")
    print ("The two random numbers are " + str(num1) + " & " + str(num2))
    if (int(num1) > int(num2)):
        print ("The higher number is: ",num1)
    else:
        print ("The higher number is: ",num2)
    print ("")

#function 6 counting the number of capital letters in your entered phrase
def fun6():
    capitalCount = 0
    for char in phrase:
        if char in alphabet:
            capitalCount = capitalCount + 1
    print ("")
    print ("There were ",capitalCount," Capital letters in your phrase.")
    print ("")

#function 7 displaying 3 random numbers and giving you the number in the middle
def fun7():
    fun7Num = []
    numb1 = (randint (0, 100))
    fun7Num.append(int(numb1))
    numb2 = (randint (0, 100))
    fun7Num.append(int(numb2))
    numb3 = (randint (0, 100))
    fun7Num.append(int(numb3))
    fun7Num.sort(reverse = True)
    print("")
    print ("The 3 numbers are ",numb1,", ",numb2," & ",numb3)
    print ("The number in the middle is ",fun7Num[1])
    print ("")

#function 8 running function 1-7 all at once
def fun8():
    print ("##################################")
    print ("            Function 1")
    print ("##################################")
    fun1()
    print ("##################################")
    print ("            Function 2")
    print ("##################################")
    fun2()
    print ("##################################")
    print ("            Function 3")
    print ("##################################")
    fun3()
    print ("##################################")
    print ("            Function 4")
    print ("##################################")
    fun4()
    print ("##################################")
    print ("            Function 5")
    print ("##################################")
    fun5()
    print ("##################################")
    print ("            Function 6")
    print ("##################################")
    fun6()
    print ("##################################")
    print ("            Function 7")
    print ("##################################")
    fun7()
    print ("##################################")
    print ("")

#asking the user which function they want to see
shouldExit = False
while shouldExit == False:
    print ("Feel free to choose another. Type 'exit' when finished.")
    print ("1 - Displays your name and Major")
    print ("2 - Prints your age as an integer")
    print ("3 - Will tell you Hello")
    print ("4 - Repeats your phrase however many times you like")
    print ("5 - Generated 2 random numbers and tells you which is larger")
    print ("6 - Returns the number of capital letters in your phrase")
    print ("7 - Generates 3 random numbers and tells you which is in the middle")
    print ("8 - Runs All Functions")
    answer = input ("Which function between 1 & 8 would you like to see? ")
    if str(answer) == "1":
        fun1()
    elif str(answer) == "2":
        fun2()
    elif str(answer) == "3":
        fun3()
    elif str(answer) == "4":
        fun4()
    elif str(answer) == "5":
        fun5()
    elif str(answer) == "6":
        fun6()
    elif str(answer) == "7":
        fun7()
    elif str(answer) == "8":
        fun8()
    elif str(answer) == "exit":
        print ("All done!!")
        shouldExit = True
    else:
        print ("Dont be dumb!")
