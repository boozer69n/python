##########################################################################
#                                  Math Tutor                            #
##########################################################################
#                           Created by: Clinton Booze                    #
#                                                                        #
#                                   Purpose:                             #
#                   To ask you multiplication problems and               #
#                     guide you in the correct direction                 #
##########################################################################
#-=SETUP=-

#random number generator
from random import randint

#defining a function to return random numbers
def getNum():
    return (randint (0, 12))

#basic variables
guesses = 0
problemCount = 0
totalProblems = 0

##########################################################################
#-=Problems=-
#asking how many problems they want to solve
shouldExit = False
while shouldExit == False:
    if (problemCount not in (1,2,3,4,5,6,7,8,9,10,11,12)):
        problemCount = input ("Please enter a number between 1 and 12 ")
        problemCount = int(problemCount)
        totalProblems = int(problemCount)
#pulling the number from the function and printing it
    else:
        numOne = getNum()
        numTwo = getNum()
        correctAnswer = numOne * numTwo
        print ("")
        print (str(numOne) + " X " + str(numTwo) + " = ?")
        yourAnswer = input ("Answer? ")
        print ("")
        yourAnswer = int(yourAnswer)
#loop checking the answer
        answerCheck = False
        while answerCheck == False:
#if answer is correct
            if (int(correctAnswer) == int(yourAnswer)):
                guesses = guesses + 1
                print ("Correct!")
                problemCount = problemCount - 1
                answerCheck = True
                if int(problemCount) == 0:
                    shouldExit = True
#if answer is too low
            elif (int(correctAnswer) > int(yourAnswer)):
                guesses = guesses + 1
                print ("Your Answer is TOO LOW")
                yourAnswer = input ("Try Again ")
                answerCheck = False
#if answer is too low
            else:
                guesses = guesses + 1
                print ("Your Answer is TOO HIGH")
                yourAnswer = input ("Try Again ")
                answerCheck = False

##########################################################################
#-=Print Out=-                        
print ("")
print ("ALL DONE!!!")
print ("You answered a total of " + str(totalProblems) + " Problems")
print ("Your average number of guesses per problem are " + str(float(guesses / totalProblems)))
print ("")
