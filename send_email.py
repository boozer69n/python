#!usr/bin/python

#imports
import smtplib
import sys
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

#takes in the raw inputs below and composes the email and sends it
#set it this way to make it so the user can enter their own email
def send_mail(me, you, pwd, sub, text):
    email_from = str(me)
    email_to = str(you)
    password = str(pwd)
    subj = str(sub)

    msg = MIMEMultipart(
        From = email_from,
        To = email_to
        )
    msg['Subject'] = subj
    msg.attach(MIMEText(text))

    server = smtplib.SMTP_SSL('smtp.gmail.com:465')
    server.login(email_from, password)
    server.sendmail(email_from, email_to, msg.as_string())
    server.close()

#written this way to only make it run if running this exact file
#to keep it from running when importing this file
if __name__ == '__main__':
    my_mail = raw_input("Your Email: ")
    my_pass = raw_input("Your Password: ")
    you_mail = raw_input("Recipiant(1 only): ")
    my_sub = raw_input("Subject: ")
    my_body = raw_input("Body Text: ")

    send_mail(my_mail, you_mail, my_pass, my_sub, my_body)
