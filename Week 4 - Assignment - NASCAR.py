#dict of the different drivers
driver1 =  {'name':'Aric Almirola',       'sponsor':'Richard Petty Motorsports', 'odometer':0, 'mph':0}
driver2 =  {'name':'Greg Biffle',         'sponsor':'Roush Fenway Racing',       'odometer':0, 'mph':0}
driver3 =  {'name':'Dave Blaney',         'sponsor':'Tommy Baldwin Racing',      'odometer':0, 'mph':0}
driver4 =  {'name':'Clint Bowyer',        'sponsor':'Michael Waltrip Racing',    'odometer':0, 'mph':0}
driver5 =  {'name':'Jeff Burton',         'sponsor':'Richard Childress Racing',  'odometer':0, 'mph':0}
driver6 =  {'name':'Kurt Busch',          'sponsor':'Furniture Row Racing',      'odometer':0, 'mph':0}
driver7 =  {'name':'Kyle Busch',          'sponsor':'Joe Gibbs Racing',          'odometer':0, 'mph':0}
driver8 =  {'name':'Landon Cassill',      'sponsor':'Circle Sport',              'odometer':0, 'mph':0}
driver9 =  {'name':'Dale Earnhardt, Jr.', 'sponsor':'Hendrick Motorsports',      'odometer':0, 'mph':0}
driver10 = {'name':'Carl Edwards',        'sponsor':'Roush Fenway Racing',       'odometer':0, 'mph':0}
driver11 = {'name':'David Gilliland',     'sponsor':'Front Row Motorsports',     'odometer':0, 'mph':0}
driver12 = {'name':'Jeff Gordon',         'sponsor':'Hendrick Motorsports',      'odometer':0, 'mph':0}
driver13 = {'name':'Denny Hamlin',        'sponsor':'Joe Gibbs Racing',          'odometer':0, 'mph':0}
driver14 = {'name':'Kevin Harvick',       'sponsor':'Richard Childress Racing',  'odometer':0, 'mph':0}
driver15 = {'name':'Jimmie Johnson',      'sponsor':'Hendrick Motorsports',      'odometer':0, 'mph':0}
driver16 = {'name':'Kasey Kahne',         'sponsor':'Hendrick Motorsports',      'odometer':0, 'mph':0}
driver17 = {'name':'Matt Kenseth',        'sponsor':'Joe Gibbs Racing',          'odometer':0, 'mph':0}
driver18 = {'name':'Brad Keselowski',     'sponsor':'Penske Racing',             'odometer':0, 'mph':0}
driver19 = {'name':'Paul Menard',         'sponsor':'Richard Childress Racing',  'odometer':0, 'mph':0}
driver20 = {'name':'Tony Stewart',        'sponsor':'Stewart-Haas Racing',       'odometer':0, 'mph':0}

#importing ability for a random number generator
from random import randint

#lists
winner = []
drivers = [driver1, driver2, driver3, driver4, driver5,
           driver6, driver7, driver8, driver9, driver10,
           driver11, driver12, driver13, driver14, driver15,
           driver16, driver17, driver18, driver19, driver20]

#function to run another minute
def race():
    for speed in drivers:
        mph = randint (1, 120)
        speed['mph'] = mph
        speed['odometer'] = (speed['odometer']) + (mph * (1/60))

#keeps running the race until its over
#and makes a list of drivers that are over 500mi
shouldExit = False
odometer = 0
while shouldExit == False:
    race()
    for distance in drivers:
        odometer = distance['odometer']
        if odometer >= 500:
            winner.append(distance)
            shouldExit = True

#narrowing down the drivers that are over 500mi
#and keeping the one with the highest odometer
base = 0
actualWinner = winner[0]
for checking in winner:
    if actualWinner['odometer'] < checking['odometer']:
        actualWinner = checking

#prints the winner and required info
print ('WINNER!!!')
print ("Name:",actualWinner['name'])
print ("Odometer:",actualWinner['odometer'])
print ("Sponsor:",actualWinner['sponsor'])
