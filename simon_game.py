#!usr/bin/python

#imports
import RPi.GPIO as GPIO
import time, random, sys

#sets up the game
def setup():
    global button_list, led_list, max_levels, buttons, cur_level, red, blue, yellow, green, seq_list, game_list
    #5x12=red
    #6x13=blue
    #17x20=yellow
    #18x21=green
    button_list = [12,13,20,21]
    led_list = [5,6,17,18]

    #variables
    max_levels = 4
    buttons = 4
    cur_level = 0

    #lights
    red = False
    blue = False
    yellow = False
    green = False

    #lists
    seq_list = []
    game_list = []

    #GPIO setup
    GPIO.setmode(GPIO.BCM)
    for i in button_list:
        GPIO.setup(i, GPIO.IN)
        GPIO.add_event_detect(i, GPIO.RISING, bouncetime = 200)
    for j in led_list:
        GPIO.setup(j, GPIO.OUT)

#clears lists and creates a new one
def new_seq():
    global cur_level
    cur_level = 1
    del seq_list[:]
    del game_list[:]
    for x in range(0,max_levels):
        seq_list.append(led_list[random.randrange(0,buttons-1)])

#checks which button was pushed
def but_chk(r):
    if r == led_list[0]:
        chk = red
        return chk
    elif r == led_list[1]:
        chk = blue
        return chk
    elif r == led_list[2]:
        chk = yellow
        return chk
    elif r == led_list[3]:
        chk = green
        return chk
    else:
        print "number out of bounds"
        return False

#pausing until a button is pressed
def but_press():
    global red, blue, yellow, green
    should_exit = False
    while should_exit == False:
        if GPIO.event_detected(button_list[0]):
            red = should_exit = True
        elif GPIO.event_detected(button_list[1]):
            blue = should_exit = True
        elif GPIO.event_detected(button_list[2]):
            yellow = should_exit = True
        elif GPIO.event_detected(button_list[3]):
            green = should_exit = True
        time.sleep(.05)

#blink the led that corresponds to the passed in argument
def blink(l):
    GPIO.output(l, True)
    time.sleep(0.2)
    GPIO.output(l, False)
    time.sleep(0.2)

#initiates the sequence blinking loop
def blink_loop():
    for i in game_list:
        blink(i)

#updates the game_list sequence
def update():
    global cur_level
    cur_level += 1
    game_list.append(seq_list.pop())

#random true/false
def tf():
    var = random.randint(0,1)
    if var == 0:
	return False
    else:
	return True
	
#randomly blink lights denothing you win
def you_win():
    print "You Win!!!!"
    for x in range(0,30):
        led = led_list[random.randint(0,buttons-1)]
	GPIO.output(led, tf())
	time.sleep(0.1)
    for y in led_list:
	GPIO.output(y, False)
		
#turn all on then all off denoting a loss
def you_lose():
    print "You Loose!!"
    for x in led_list:
	GPIO.output(x, True)
	time.sleep(0.1)
    for y in led_list:
	GPIO.output(y, False)
	time.sleep(0.1)

#starting a new game
def round():
    global red, blue, yellow, green
    update()
    blink_loop()
    correct = True
    location = 0
    while correct:
	if location >= len(game_list):
	    return True
	but_press()
	correct = but_chk(game_list[location])
	red = blue = yellow = green = False
	location += 1
    return False

#run the game
def game():
    new_seq()
    loop = True
    while (loop) and (max_levels >= cur_level):
        print "current level: ", cur_level
	loop = round()
    if loop:
	you_win()
    else:
        you_lose()
		
if __name__ == '__main__':
    while True:
        choice = input("1: Play Game \n0: Exit \nChoice: ")
        if choice == 0:
            GPIO.cleanup
            sys.exit("Goodbye!")
        else:
            setup()
            game()
            GPIO.cleanup()
